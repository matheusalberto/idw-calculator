export const operatorsEnum = {
  UNDEFINED: 0,
  ADD: 1,
  SUB: 2,
  TIMES: 3,
  DIV: 4,
  EQUALS: 5
}

export function add (a, b) { return a + b }
export function sub (a, b) { return a - b }
export function div (a, b) { return a / b }
export function mult (a, b) { return a * b }
